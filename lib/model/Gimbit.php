<?php

class Gimbit{

  private $_id;
  private $_gmbttxt;
  private $_user;

  public function __construct($txt, $user, $id = null){
    $this->setGimbitTxt($txt);
    $this->setUser($user);
    $this->setId($id);
  }

  public function setId($var){
    $this->_id = $var;
  }

  public function setGimbitTxt($var){
    $this->_gmbttxt = $var;
  }
  
  public function setUser($var){
    $this->_user = $var;
  }

  public function getId(){
    return $this->_id;
  }

  public function getGimbitTxt(){
    return $this->_gmbttxt;
  }

  public function getUser(){
    return $this->_user;
  }

  public function toArray(){
    $ret = [
      "gimbit" => $this->getGimbitTxt(),
      "user" => $this->getUser()->toArray() ];
    if($this->getId() != null){
      $ret["id"] = $this->getId();
    }
  }

}