<?php

class UserDb{

  public function insertUser($usr){
    $collection = $this->openConnection();
    $insertusr = $collection->insertOne($usr->toArray());

    return $insertusr->getInsertedId();
  }

  private function openConnection(){
    return (new MongoDB\Client)->gimbit->users;
  }

}