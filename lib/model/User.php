<?php

class User{

  private $_id;
  private $_username;
  private $_password;
  private $_realname;

  public function __construct($uname, $rname, $id = null, $pwd = null){
    $this->setUsername($uname);
    $this->setRealname($rname);
    $this->setId($id);
    $this->setPassword($pwd);
  }

  public function setId($var){
    $this->_id = $var;
  }

  public function setUsername($var){
    $this->_username = $var;
  }

  public function setPassword($var){
    $this->_password = $var;
  }

  public function setRealname($var){
    $this->_realname = $var;
  }

  public function getId(){
    return $this->_id;
  }

  public function getUsername(){
    return $this->_username;
  }

  public function getPassword(){
    return $this->_password;
  }

  public function getRealname(){
    return $this->_realname;
  }

  public function toArray(){
    $ret = [
        "username" => $this->getUsername(),
        "realname" => $this->getRealname() ];
    if($this->getPassword() != null){
      $ret["pwd"] = $this->getPassword();
    }
    if($this->getId() != null){
      $ret["id"] = $this->getId();
    }
    return $ret;
  }

}