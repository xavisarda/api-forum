<?php

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Factory\AppFactory;
use Slim\Middleware\BodyParsingMiddleware;

require_once(__DIR__ . '/../lib/ext/vendor/autoload.php');
require_once(__DIR__.'/../lib/controller/UserController.php');
require_once(__DIR__.'/../lib/controller/GimbitController.php');

$app = AppFactory::create();
$app->addErrorMiddleware(true, false, false);
$app->addBodyParsingMiddleware();

// Gimbit scope

$app->get('/gimbit', function (Request $request, Response $response, array $args) {
    //return gimbit list
});

$app->post('/gimbit', function (Request $request, Response $response, array $args) {
    //publish gimbit
});

$app->delete('/gimbit/{gid}', function (Request $request, Response $response, array $args) {
    //delete gimbit
});

// User scope

$app->post('/user', function (Request $request, Response $response, array $args) {
    $cnt = new UserController();
    $dades = $request->getParsedBody();
    $insertedId = $cnt->createUser($dades['username'], $dades['realname'], $dades['pwd']);
    $retDades = ["uid" => $insertedId];

    $response = $response->withHeader('Content-type', 'application/json');
    $response->getBody()->write(json_encode($retDades));
    return $response;
});

$app->put('/user/{uid}', function (Request $request, Response $response, array $args) {
    //update user
});

$app->get('/user/{uid}', function (Request $request, Response $response, array $args) {
    //retrieve user info
});

$app->get('/user/login/{uid}', function (Request $request, Response $response, array $args) {
    //login user
});

$app->run();
