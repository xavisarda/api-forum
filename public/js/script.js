
$(document).ready(function(){
  $('#ubtnreg').click(function(){
    userReg();
  });
  $('#confirmationcnt').hide();
  $('#errorcnt').hide();
});

function userReg(){
  var userdata = { "username" : $('#unamereg').val(),
                "realname": $('#urealnreg').val(),
                "pwd" : $('#upwdreg').val() };
  $.ajax({
    accepts: 'json',
    contentType: 'application/json',
    data: JSON.stringify(userdata),
    dataType: "json",
    url: '/user',
    method: 'POST',
    success: function(data){
      showConfirmationMsg('User created successfully');
    },
    error: function(){
      showErrorMsg('User registration failed')
    }
  });
}

function showConfirmationMsg(txt){
  var msg = '<span style="color:green;" onclick="hideConfirmationMsg()">'+txt+'</span>';
  $('#confirmationcnt').html(msg);
  $('#confirmationcnt').show();
}

function hideConfirmationMsg(){
  $('#confirmationcnt').hide();
  $('#confirmationcnt').html('');
}

function showErrorMsg(txt){
  var msg = '<span style="color:red;" onclick="hideErrorMsg()">'+txt+'</span>';
  $('#errorcnt').html(msg);
  $('#errorcnt').show();
}

function hideErrorMsg(){
  $('#errorcnt').hide();
  $('#errorcnt').html('');
}